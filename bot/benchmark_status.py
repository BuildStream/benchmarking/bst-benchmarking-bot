#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

"""Variables that are set in the benchmarking pipeline
   "BS_MR" - The buildstream MR reference that triggered the benchmarking pipeline.
   "BS_BRANCH" - The buildstream branch that will be benchmarked.
   "BS_REPO_PATH" - The buildstream repository path - allows the use of a test repo for example.
   "BS_PIPE_ID" - The buildstream pipeline id whose successful completion triggered the
                  benchmarking pipeline run.
   "BS_PIPE_SHA" - The sha reference for the buildstream commit at the head of the buildstream
                   branch at the point at which the benchmarking pipeline is triggered.
   "BS_PIPE_START_TIME" - The start time for the buildstream pipeline that triggered the
                          benchmarking run.
   "BS_PIPE_END_TIME" - The finish time for the buildstream pipeline that triggered the
                        benchmarking run.
   "BS_NOTE" - The buildstream MR note reference that is used to reflect the benchmarking pipeline
               status.
   "NO_VERIFICATION" - This allows an override of the benchmarking check on the validity of the
                       nightly run token. If the token is inconsistent with the current benchmarking
                       cache status then the benchmarking pipeline will fail before performing any
                       benchmarking. If the token check is removed the benchmarking pipeline can be
                       re-run manually without a further MR triggering process.
"""

PIPELINE_VARS = {'buildstream_mr_ref' : "BS_MR",
                 'buildstream_branch_ref' : "BS_BRANCH",
                 'buildstream_repo_ref' : "BS_REPO_PATH",
                 'buildstream_pipeline_id' : "BS_PIPE_ID",
                 'buildstream_pipeline_sha' : "BS_PIPE_SHA",
                 'buildstream_pipeline_trigger_time' : "BS_PIPE_START_TIME",
                 'buildstream_pipeline_end_time' : "BS_PIPE_END_TIME",
                 'buildstream_mr_note_ref' : "BS_NOTE",
                 'benchmarking_no_token_check' : "NO_VERIFICATION"
                }


class BenchmarkPipelineEntryStatus:
    """Encapsulate the status of a single benchmark pipeline entry,
      this needs to take into account variables set for corresponding
      buildstream MR id references.
   """

    def __init__(self, bm_pipeline):
        """Create the benchmarking status object.

         Args:
            bm_pipeline: A single benchmark pipeline to be updated or
                         have data extracted.
      """
        self._bm_pipeline = bm_pipeline
        # pylint: disable=unnecessary-comprehension
        self._variables = [variable for variable in bm_pipeline.variables.list()]

    def get_pipeline_variable(self, ref):
        id_ref = list(
            filter(
                lambda variable: variable.value if variable.get_id() == ref else None,
                self._variables,
            )
        )
        rtn = None
        if id_ref:
            rtn = id_ref[0].value
        return rtn

    def get_buildstream_mr(self):
        """Get the buildstream reference
      """
        mr_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_mr_ref'])
        return mr_ref

    def get_buildstream_branch(self):
        """Get the buildstream branch
      """
        mr_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_branch_ref'])
        return mr_ref

    def get_buildstream_repo(self):
        """Get the buildstream repo
      """
        mr_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_repo_ref'])
        return mr_ref

    def get_buildstream_pipeline_id(self):
        """Get the buildstream pipeline id
      """
        id_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_pipeline_id'])
        return id_ref

    def get_buildstream_pipeline_sha(self):
        """Get the buildstream pipeline sha
      """
        sha_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_pipeline_sha'])
        return sha_ref

    def get_buildstream_pipeline_bs_pipe_start_time(self):
        """Get the buildstream pipeline trigger time
      """
        time_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_pipeline_trigger_time'])
        return time_ref

    def get_buildstream_pipeline_bs_pipe_end_time(self):
        """Get the buildstream pipeline end time
      """
        time_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_pipeline_end_time'])
        return time_ref

    def get_buildstream_mr_note_ref(self):
        """Get the buildstream mr note ref
      """
        note_ref = self.get_pipeline_variable(PIPELINE_VARS['buildstream_mr_note_ref'])
        return note_ref

    @property
    def benchmarking_pipeline(self):
        """Get the benchmarks pipeline object
      """
        return self._bm_pipeline


class BenchmarkRequest:
    """ Encapsulate Benchmarking requests.
   """


    def __init__(self, config):
        """Create the benchmarking request object.

         Args:
            config: A single benchmark configuration.
      """
        self._config = config
        self._bsmrnoteid = None

        try:
            self._bsmrnoteid = getattr(config, "BsMRNoteId")
        except AttributeError:
            self._bsmrnoteid = None

    def set_bs_mr_note_id(self, noteid):
        self._bsmrnoteid = noteid

    @property
    def get_bs_mr_note_id(self):
        return self._bsmrnoteid

    @property
    def get_config(self):
        return self._config

    def variable_dict(self):
        return {PIPELINE_VARS['buildstream_mr_ref'] : str(self._config.BsMrId),
                PIPELINE_VARS['buildstream_branch_ref'] : self._config.BsBranchRef,
                PIPELINE_VARS['buildstream_repo_ref'] : self._config.BsRepoRef,
                PIPELINE_VARS['buildstream_pipeline_id'] : str(self._config.BsPipelineId),
                PIPELINE_VARS['buildstream_pipeline_sha'] : self._config.BsBranchSHA,
                PIPELINE_VARS['buildstream_pipeline_trigger_time'] :
                self._config.TriggerTimeOfBsPipeline,
                PIPELINE_VARS['buildstream_pipeline_end_time'] :
                self._config.FinishTimeOfBsPipeline,
                PIPELINE_VARS['buildstream_mr_note_ref'] :
                self._bsmrnoteid,
                PIPELINE_VARS['benchmarking_no_token_check'] : 'TRUE'
               }
