#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import logging
import gitlab


def parse_pipelines(config, project, _gl=None):
    """ This will get a list of pipelines for a given project.

       Args:
            config - the parsed configuration for the bot.
            gl - (optional) dev environment reference.
            project - the project that the required pipelines pertain to.
       Returns:
            list of pipelines.
   """

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(project)
    # Get pipelines list
    pipelines = project.pipelines.list()
    detailed_pipelines = [project.pipelines.get(pipeline.id) for pipeline in pipelines]
    return detailed_pipelines


def get_or_create_trigger(project):
    trigger_description = 'bot_benchmarking'
    for trg in project.triggers.list():
        if trg.description == trigger_description:
            return trg
    return project.triggers.create({'description': trigger_description})


def trigger_pipeline(config, bm_config, _gl=None):
    """ This will trigger a pipeline for a given benchmark request using the
        parameters encapsulated in a BenchmarkRequest.

       Args:
            config - the parsed configuration for the bot.
            bm_config - the configuration for the benchmarking requested.
            gl - (optional) dev environment reference.
       Returns:
            Success or failure.
   """
    if config is None:
        logging.error(
            "No parsed bot configuration to use in triggering pipeline"
        )

    if bm_config is None:
        logging.error(
            "No parsed benchmarking configuration to use in triggering of pipeline"
        )

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to benchmark project instance
    project = _gl.projects.get(config.get_bm_repo)

    # Get trigger
    trigger = get_or_create_trigger(project)

    # Create pipeline
    pipeline = project.trigger_pipeline('master',
                                        trigger.token,
                                        variables=bm_config.variable_dict())

    # Return the pipeline id
    return pipeline
