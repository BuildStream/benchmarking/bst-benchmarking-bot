#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import logging
import yaml


class BotConfig:
    """Specifies the bot configuration.

   The bot configuration can be via command line parameters:q
   or a configuration file.

   Args:
       *args
       **kwargs
   """

    class default:  # pylint: disable=invalid-name
        """Specifies the defaults for the bot configuration.

      All static values retained for central control

      """

        @staticmethod
        def debug():
            """Whether debugging is enabled."""
            return False

        @staticmethod
        def logging_path():
            """What logging path should be used."""
            return None

        @staticmethod
        def dev_host():
            """What the path is to the development environment host."""
            return "https://gitlab.com"

        @staticmethod
        def bs_repo_path():
            """What the buildstream repo under test is called."""
            return "buildstream"

        @staticmethod
        def bm_repo_path():
            """What the benchmarking repo being used is called
            Note: It is assumed that both buildstream and
                  benchmarking are hosted in the same environment.
         """
            return "Buildstream/benchmarks"

        @staticmethod
        def bs_bm_tags():
            """The MR tags that are used to denote benchmarking is required
            for associated branch.
         """
            return ["Benchmark_Prio-1", "Benchmark_Prio-2"]

        @staticmethod
        def token():
            """The API access token for the dev host."""
            return ""

        @staticmethod
        def runner_id():
            """The ID for the dev. runner environment (gitlab runner)."""
            return "bl-benchmark-runner-1"

        @staticmethod
        def runner_tags():
            """The tags associated with the benchmarking pipeline run."""
            return ["cleanup", "benchmarks", "publish"]

        @staticmethod
        def timeout():
            """The timeout between polling instances."""
            return 10

    def __init__(self, **kwargs):

        self._debug = BotConfig.default.debug()
        self._logging_path = BotConfig.default.logging_path()
        self._dev_host = BotConfig.default.dev_host()
        self._bs_repo_path = BotConfig.default.bs_repo_path()
        self._bm_repo_path = BotConfig.default.bm_repo_path()
        self._bs_bm_tags = BotConfig.default.bs_bm_tags()
        self._token = BotConfig.default.token()
        self._runner_id = BotConfig.default.runner_id()
        self._runner_tags = BotConfig.default.runner_tags()
        self._timeout = BotConfig.default.timeout()

        if "config_file" in kwargs and kwargs["config_file"] is not None:
            self.__process_file(file_path=kwargs["config_file"])
        else:
            self.__process_command_line(**kwargs)

    def __repr__(self):

        _str = "".join(
            [
                "Configuration - Debug: {}, Logging Path: {}, Development Host: {}, ",
                "Buildstream Project Path: {}, Benchmarking Project Path: {}, ",
                "Buildstream Benchmarking MR Tags: {}, Development Token Access: {}, ",
                "Benchmarking Runner Id: {}, Benchmarking Runner Tags: {}, ",
                "Polling timeout: {}",
            ]
        )

        return _str.format(
            self._debug,
            self._logging_path,
            self._dev_host,
            self._bs_repo_path,
            self._bs_bm_tags,
            self._bm_repo_path,
            self._token,
            self._runner_id,
            self._runner_tags,
            self._timeout,
        )

    def __process_file(self, file_path):
        """Process configuration.
         Reads and checks configuration file

      Args:
         file_path: Path to the configuration file to be processed.
      """
        valid_keys = {
            "config",
            "debug",
            "logging_path",
            "dev_host",
            "bs_repo",
            "bs_bm_tags",
            "bm_repo",
            "token",
            "runner_id",
            "runner_tag",
            "timeout",
        }

        try:
            with open(file_path) as f:
                config_f = yaml.safe_load(f)
        except yaml.error.YAMLError as e:
            logging.error("Invalid YAML syntax in configuration: %s", e)

        config = config_f["config"]
        invalid_keys = set(config.keys()) - valid_keys

        if invalid_keys:
            logging.error(
                "Invalid keys in configuration: %s", ''.join(invalid_keys)
            )

        if "debug" in config.keys():
            if config["debug"] == "True":
                self._debug = True
            else:
                self._debug = False
        else:
            self._debug = False

        if "logging_path" in config.keys():
            self._logging_path = config["logging_path"]

        if "dev_host" in config.keys():
            self._dev_host = config["dev_host"]

        if "bs_repo" in config.keys():
            self._bs_repo_path = config["bs_repo"]

        if "bs_bm_tags" in config.keys():
            self._bs_bm_tags = config["bs_bm_tags"]

        if "bm_repo" in config.keys():
            self._bm_repo_path = config["bm_repo"]

        if "token" in config.keys():
            self._token = config["token"]

        if "runner_id" in config.keys():
            self._runner_id = config["runner_id"]

        if "runner_tag" in config.keys():
            self._runner_tags = config["runner_tag"]

        if "timeout" in config.keys():
            self._timeout = config["timeout"]

    def __process_command_line(self, **kwargs):
        """Process commandline options.
         Parses passed click commandline options (see __main__.py).
         Checks that configuration makes sense.

      Args:
         commandline args and kwargs.
      """

        if "debug" in kwargs:
            if kwargs["debug"] == "True":
                self._debug = True
            else:
                self._debug = False
        else:
            self._debug = False

        if "logging_path" in kwargs:
            self._logging_path = kwargs["logging_path"]

        if "dev_host" in kwargs:
            self._dev_host = kwargs["dev_host"]

        if "bs_repo_path" in kwargs:
            self._bs_repo_path = kwargs["bs_repo_path"]

        if "bs_bm_tags" in kwargs:
            self._bs_bm_tags = kwargs["bs_bm_tags"]

        if "bm_repo_path" in kwargs:
            self._bm_repo_path = kwargs["bm_repo_path"]

        if "token" in kwargs:
            self._token = kwargs["token"]

        if "runner_id" in kwargs:
            self._runner_id = kwargs["runner_id"]

        if "runner_tags" in kwargs:
            self._runner_tags = kwargs["runner_tags"]

        if "timeout" in kwargs:
            self._timeout = kwargs["timeout"]

    @property
    def get_debug(self):
        return self._debug

    @property
    def get_logging_path(self):
        return self._logging_path

    @property
    def get_dev_host(self):
        return self._dev_host

    @property
    def get_bs_repo(self):
        return self._bs_repo_path

    @property
    def get_bs_bm_tags(self):
        return self._bs_bm_tags

    @property
    def get_bm_repo(self):
        return self._bm_repo_path

    @property
    def get_token(self):
        return self._token

    @property
    def get_runner_id(self):
        return self._runner_id

    @property
    def get_runner_tag(self):
        return self._runner_tags

    @property
    def get_timeout(self):
        return self._timeout
