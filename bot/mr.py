#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

from datetime import datetime

import csv
import logging
import urllib.request

from operator import itemgetter

import gitlab
import requests
import tabulate

from .benchmark_status import BenchmarkRequest

BS_MR_MESSAGES = {'bm_run_accepted' : "Benchmark run accepted, pipeline link is: {}",
                  'bm_run_status_failed' : "Benchmark run status shows failed",
                  'bm_request_accepted' : 'Benchmarking request accepted: {}\n',
                  'bm_run_failed' : "Benchmark run failed: Pipeline {}",
                  'bm_run_success' : "Benchmark run success: Pipeline {}",
                  'bm_rm_label' : "Benchmarking remove labels MR: {} " \
                                  "in response to Benchmarking pipeline {}",
                  'bm_results_dg' : "Digest of results can be found at: {}",
                  'bm_digest_er' : "No consolidated digest results",
                  'bm_digest_download_fail' : "Unable to download results",
                  'bm_unable_to_read_results' : "Unable to read results",
                  'bm_results_enexpected' : "Results are in unexpected format"
                 }

def parse_mrs(config, _gl=None):
    """ This will get a list of project MRs that are currently tagged
       for benchmarking.

       Args:
            config - the parsed configuration for the bot.
            _gl - the gitlab reference (optional)
       Returns:
            list of buildstream MRs that are tagged for benchmarking.
   """

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(config.get_bs_repo)
    # Get merge request list
    mrs = project.mergerequests.list(state='all', order_by='updated_at')
    benchmarking_mrs = []
    # Iterate merge requests
    for _mr in mrs:
        # Check if MR contains a benchmarking tag in its labels
        if any(i in _mr.labels for i in config.get_bs_bm_tags):
            # Append MRs with overlap to list
            benchmarking_mrs.append(_mr)
    # Return list of MRs tagged with benchmarking labels
    return benchmarking_mrs


def add_mr_note(config, benchmark_request, _gl=None):
    """ This will add a discussion note to the project MRs to denote a benchmark
       run is in the process of being requested. This acts as a placeholder entry
       which will be updated once the benchmark run has been requested and when
       the benchmarking results are available.

       Args:
            config - the parsed configuration for the bot.
            benchmark_request - the requested benchmark configuration.
            _gl - the gitlab reference
       Returns:
            note id
   """
    if benchmark_request is None:
        logging.error("Unable to add note: No request to process")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(config.get_bs_repo)
    mrs = project.mergerequests.list(state='all', order_by='updated_at')
    # Get mr reference
    mr_ref = next(x for x in mrs if x.id == int(benchmark_request.get_config.BsMrId))
    discussion = mr_ref.discussions.create({'body':
                                            BS_MR_MESSAGES['bm_request_accepted']
                                            .format(datetime.now())})
    note_id = discussion.attributes['id']
    discussion.resolved = False
    discussion.save()
    mr_ref.save()
    logging.info("Benchmarking request accepted MR: %s", benchmark_request.get_config.BsMrId)
    return note_id


def update_mr_note(config, benchmark_run, text, _gl=None):
    """ This will provide a generic way to update a pre-exisiting discussion note
       run.

       Args:
            config - the parsed configuration for the bot.
            benchmark_run - details of the benchmarking run request.
            text - the text that is to be appended to the discussion note.
            _gl - the gitlab reference
       Returns:
            updated note id
   """
    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if benchmark_run is None:
        logging.error("No benchmarking run given")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(config.get_bs_repo)
    # Get mr reference
    mrs = project.mergerequests.list(state='all', order_by='updated_at')
    mr_ref = next((x for x in mrs if x.id == int(benchmark_run.get_config.BsMrId)), None)
    if not mr_ref:
        logging.error("unable to find expected MR reference %s", benchmark_run.get_config.BsMrId)
        return None
    # Get discussion ref.
    try:
        disscussion_update = mr_ref.discussions.get(benchmark_run.get_bs_mr_note_id)
    except gitlab.exceptions.GitlabError:
        logging.error("Unable to access MR %s note: %s",
                      benchmark_run.get_config.BsMrId, benchmark_run.get_bs_mr_note_id)
        return None
    # Get latest note in discussions ID
    set_note_id = disscussion_update.attributes['notes'][-1]['id']
    last_note = disscussion_update.notes.get(set_note_id)
    # Add update comment
    last_note.body = last_note.body + '\n' + text
    # Save note
    try:
        result = last_note.save()
    except gitlab.exceptions.GitlabUpdateError:
        logging.error("Unable to update note %s in MR %s",
                      set_note_id, benchmark_run.get_config.BsMrId)
        return None
    # Return result
    return result


def update_mr_run(config, status, benchmark_run, _gl=None):
    """ This will update a discussion note with details about a requested benchmark
       run. This will update the note with a link to the benchmarking pipeline and
       a time stamp for the pipeline triggering. A note id is returned which will
       be added to the benchmark run pipeline variables.

       Args:
            config - the parsed configuration for the bot.
            status - the got status.
            benchmark_run - details of the benchmarking run request.
            _gl - the gitlab reference
       Returns:
            note id
   """
    text = ''

    if status is None:
        text = ("Benchmark run trigger failed")
        logging.error("benchmark run trigger failed")
    else:
        if status.get_pipeline_status() == 'failed':
            text = BS_MR_MESSAGES['bm_run_status_failed']
            logging.error(BS_MR_MESSAGES['bm_run_status_failed'])
        else:
            # Add update comment with link to the benchmarking pipeline
            text = (BS_MR_MESSAGES['bm_run_accepted']).format(status.get_pipeline_weblink())
    # Add update
    updated_note = update_mr_note(config, benchmark_run, text, _gl)

    logging.info("Benchmarking request for MR: %s, pipeline link: %s",
                 benchmark_run.get_config.BsMrId,
                 status.get_pipeline_weblink())
    return updated_note


def update_mr_failure(config, benchmark_run, _gl=None):
    """ This will update a discussion note with details about a benchmark failure.
       Initially this will simply update the note with a simple failure statement
       and it will be up to a user to review the pipeline information via the
       weblink already annotated in the MR.

       Args:
            config - the parsed configuration for the bot.
            status - the got status.
            benchmark_run - details of the benchmarking run request.
            _gl - the gitlab reference
       Returns:
            note id
   """
    # Add update comment with link to the benchmarking pipeline
    text = (BS_MR_MESSAGES['bm_run_failed']).format(benchmark_run.get_config.BmPipelineId)
    # Add update
    updated_note = update_mr_note(config, benchmark_run, text, _gl)
    return updated_note


def update_mr_results(config, status, benchmark_complete, _gl=None):
    """ This will update project MRs against benchmarking results.
       This will not update for triggered benchmarking.

       Args:
            config - the parsed configuration for the bot.
            status - the got status.
            benchmark_complete - the completed benchmark.
            _gl - the gitlab reference
       Returns:
            list of buildstream MRs that are tagged for benchmarking.
   """

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if status is None:
        logging.error("No status provided")
        return None

    if benchmark_complete is None:
        logging.error("No completed benchmark provided")
        return None

    benchmark_result = BenchmarkRequest(benchmark_complete)

    if benchmark_complete.BmPipelineStatus == "success":
        logging.info("Benchmarking success %s", benchmark_complete.BsPipelineId)
        text = BS_MR_MESSAGES['bm_run_success'].format(benchmark_complete.BsPipelineId)
        update_mr_note(config, benchmark_result, text, _gl)
        start_time = benchmark_complete.TriggerTimeOfBmPipeline
        finish_time = benchmark_complete.FinishTimeOfBmPipeline
        text = "Benchmarking started at {}, complete at {}".format(start_time, finish_time)
        update_mr_note(config, benchmark_result, text, _gl)
    else:
        logging.info("Benchmarking failed %s", benchmark_complete.BsPipelineId)
        update_mr_failure(config, benchmark_result, _gl)

    return benchmark_result


def check_for_note_entry(config, benchmark_run, text, _gl=None):
    """ Check if note entry exists in MR discussion.

       Args:
            config - the parsed configuration for the bot.
            benchmark_run - the benchmarking run config.
            text - The expected note
            _gl - the gitlab reference
       Returns:
            boolean true if exists.
   """
    if benchmark_run is None:
        logging.error("No status to benchmark run provided")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if text is None:
        logging.error("No text entry specified available unable to check")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )

    # Get reference to project instance
    project = _gl.projects.get(config.get_bs_repo)
    # Get mr reference
    mrs = project.mergerequests.list(state='all', order_by='updated_at')
    mr_ref = next(x for x in mrs if x.id == int(benchmark_run.BsMrId))
    # Check if update has already been added to notes
    disscussion = mr_ref.discussions.get(benchmark_run.BsMRNoteId)

    notes = disscussion.attributes['notes']

    ret = False
    for note in notes:
        print(note)
        if text in note['body']:
            ret = True
    return ret


def remove_mr_labels(config, benchmark_run, _gl=None):
    """ This will remove benchmarking labels in MRs.

       Args:
            config - the parsed configuration for the bot.
            benchmark_run - the benchmarking run config.
            _gl - the gitlab reference
       Returns:
            list of buildstream MRs that are tagged for benchmarking.
   """

    mr_updates = True

    if benchmark_run is None:
        logging.error("No status to benchmark run provided")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(config.get_bs_repo)
    # Get mr reference
    mrs = project.mergerequests.list(state='all', order_by='updated_at')
    mr_ref = next(x for x in mrs if x.id == int(benchmark_run.BsMrId))

    # Text used to annotate MR when labels are removed
    text = BS_MR_MESSAGES['bm_rm_label'].format(mr_ref.id, benchmark_run.BmPipelineId)
    # Check if already removed in response to MR and pipeline
    if check_for_note_entry(config, benchmark_run, text, _gl):
        logging.info("Label already removed in response " \
                     "to results in pipeline %s, skipping", benchmark_run.BmPipelineId)
        return None

    # Get the current MR labels
    labels = mr_ref.labels
    # Remove labels set for benchmarking from MR label set
    mr_ref.labels = [x for x in labels if x not in config.get_bs_bm_tags]
    # Save the updated label set to the MR
    try:
        mr_ref.save()
    except gitlab.exceptions.GitlabCreateError as e:
        logging.error("Unable to update MR %s with result: error %s", mr_ref.id, e)
        mr_updates = False

    logging.info("Benchmarking remove labels MR: %s", benchmark_run.BsMrId)

    update_mr_note(config, BenchmarkRequest(benchmark_run), text, _gl=None)

    return mr_updates


def check_for_update(status, benchmark_complete):
    """ This will check if an MR has already been updated with a result.

       Args:
            config - the parsed configuration for the bot.
            pipeline_id - the benchmarking pipeline id that is completed.
            _gl - the gitlab reference
       Returns:
            boolean - true if update to MR still required
   """

    ret = True

    # Get MR reference
    mr_ref = next((x for x in status.bs_mrs
                   if x.id == int(benchmark_complete.BsMrId)), None)

    # If MR no longer in pool tagged for benchmarking no need to update.
    if mr_ref is None:
        logging.debug("MR %s no longer flagged for benchmarking no update",
                      benchmark_complete.BsMrId)
        return None

    # Check if update has already been added to notes
    disscussion = mr_ref.discussions.get(benchmark_complete.BsMRNoteId)

    notes = disscussion.attributes['notes']

    success = BS_MR_MESSAGES['bm_run_success'].format(benchmark_complete.BmPipelineId)
    failure = BS_MR_MESSAGES['bm_run_failed'].format(benchmark_complete.BmPipelineId)
    status_fail = BS_MR_MESSAGES['bm_run_status_failed']
    label_removed = BS_MR_MESSAGES['bm_rm_label'] \
                    .format(benchmark_complete.BsMrId, benchmark_complete.BmPipelineId)
    for note in notes:
        logging.info("Note is : %s", note['body'])
        if (success in note['body']) or \
           (failure in note['body']) or \
           (status_fail in note['body']) or \
           (label_removed in note['body']):
            ret = False

    return ret

def create_markdown_table(results, benchmark_info):
    # Resolved list of results
    results_dict = []
    # Iterate sorted results
    for item in results:
        # Take a copy of the result entry
        item_cp = item
        # If error type set results fields to reflect
        if item['Error Type']:
            item_cp['Average Time to Complete Test (s)'] = 'ERROR'
            item_cp['Time Standard Deviation'] = 'ERROR'
        # If SHA is for the feature branch being tested, set the versioning information accordingly.
        if item['SHA'] == benchmark_info.BsBranchSHA:
            item_cp['Test Version'] = benchmark_info.BsBranchRef
        else:
            # Strip out the repo information and set the test version accordingly
            item_cp['Test Version'] = item['Test Version'].split(sep='.', maxsplit=1)[0]
        # Truncate the sha reference to be a little less verbose
        item_cp['SHA'] = item['SHA'][:9]
        # Add the modified item to the list to be displayed
        results_dict.append(item_cp)
    # Create the Markdown table from the results
    dmt_table = tabulate.tabulate(results_dict, headers="keys", tablefmt="pipe")
    # Return result
    return dmt_table

def post_digest_results(config, benchmark_result, _gl=None):

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get reference to project instance
    project = _gl.projects.get(config.get_bm_repo)
    # Get the pipeline reference
    pipeline = project.pipelines.get(benchmark_result.BmPipelineId)
    # Get the jobs
    jobs = pipeline.jobs.list(scope='success')
    # Look for the final job that publishes results
    publish_job_ref = None
    for job in jobs:
        if job.stage == 'deploy':
            publish_job_ref = job

    benchmark_req = BenchmarkRequest(benchmark_result)

    if not publish_job_ref:
        error_msg = BS_MR_MESSAGES['bm_digest_er']
        logging.error(error_msg)
        update_mr_note(config, benchmark_req, error_msg, _gl)
        return None

    # Get the artifact
    job_path = publish_job_ref.web_url
    url_path = ''.join([job_path, '/artifacts/raw/pipeline_cache/all_results_digest.csv'])
    # Check if expected digest results exist
    request = requests.get(url_path)
    if request.status_code == 200:
        logging.info("Digest file exists at: %s", url_path)
    else:
        logging.error("Digest file does not exist at: %s", url_path)
        fail_note = BS_MR_MESSAGES['bm_digest_er']
        update_mr_note(config, benchmark_req, fail_note, _gl)
        return None
    logging.info("Updating mr with results path: \n %s", url_path)
    # Create master dictionary for results
    csv_dict = []
    # Get file
    try:
        local_filename, _header = urllib.request.urlretrieve(url_path)
        # Open file and read as dictionary
        with open(local_filename, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            # Create master dictionary of results
            for row in reader:
                csv_dict.append(row)
    except (urllib.error.URLError, urllib.error.ContentTooShortError) as e:
        logging.error("Unable to download results file: %s: error: %s", url_path, e)
        fail_note = BS_MR_MESSAGES['bm_digest_download_fail']
        update_mr_note(config, benchmark_req, fail_note, _gl)
        return None
    except IOError as err:
        logging.error("Unable to read results file: %s", err)
        fail_note = BS_MR_MESSAGES['bm_unable_to_read_results']
        update_mr_note(config, benchmark_req, fail_note, _gl)
        return None
    finally:
        if local_filename:
            # Cleanup
            urllib.request.urlcleanup()

    try:
        # Sort list to get suitable ordering
        sort_dict = sorted(csv_dict, key=itemgetter('Test Name', 'Job ID', 'Test Version'))
        # Create the markdown table and update mr
        table = create_markdown_table(sort_dict, benchmark_result)
        # Add lines before and after table to ensure formatting
        mr_table = ''.join(['\n\n', table, '\n\n'])
        update_mr_note(config, benchmark_req, mr_table, _gl)
    except KeyError:
        logging.error("Results are of incorrect format - continue")
        fail_note = BS_MR_MESSAGES['bm_results_enexpected']
        update_mr_note(config, benchmark_req, fail_note, _gl)

    note = BS_MR_MESSAGES['bm_results_dg'].format(url_path)
    update_mr_note(config, benchmark_req, note, _gl)
    logging.info("Results annotated to MR %s", benchmark_result.BsMrId)
    return url_path
