#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import logging

from collections import namedtuple
from datetime import datetime
from .benchmark_status import BenchmarkPipelineEntryStatus


class Status:
    """ Encapsulates the derived status for benchmarking via gitlab
       runners.
   """

    def __init__(self, config, bs_mrs, bs_pipelines, bm_pipelines, bm_runners):
        """Create the status object.

         Args:
            config: The underlying configuration being used
            bs_mrs: A list of buildstream MRs that are labeled for benchmarking
            bs_pipelines: A list of buildstream pipelines that associate with
                          MRs that are labeled for benchmarking.
            bm_pipelines: A list of benchmarking pipelines that associate with
                          benchmarking.
            bm_runners: A list of gitlab runners that have sufficient job tags
                        to allow benchmarking ci runs to take place.
      """
        self._config = config
        self._bs_mrs = bs_mrs
        self._bs_repo = ''.join([config.get_dev_host, '/', config.get_bs_repo])
        self._bs_pipelines = bs_pipelines
        self._bm_pipelines = bm_pipelines
        self._bm_runners = bm_runners
        self._available_runners = self.get_idle_runners()
        self._benchmarking_pending = self.get_pending_benchmarking()
        self._benchmarking_ready = self.get_ready_benchmarking()
        self._benchmarking_running = self.get_running_benchmarking()
        self._benchmarking_complete = self.get_complete_benchmarking()

    @staticmethod
    def get_time(str_time):
        """Gets a datetime object from a passed gitlab string
      """
        return datetime.strptime(str_time, "%Y-%m-%dT%H:%M:%S.%f%z")

    def get_idle_runners(self):
        """Determine which runners are spare to carry out new benchmarking
         tasks
      """
        # Filtering options runners/pipelines that have running or pending jobs
        opts = ("running", "pending")
        # Get all pipelines that are either running or pending
        pending_pipelines = [
            pipeline for pipeline in self._bm_pipelines if pipeline.status in opts
        ]
        # If the number of pipelines already scheduled is equal to or larger than the
        # number of valid runners available then return empty list (none spare)
        idle_runners = []
        if len(pending_pipelines) < len(self._bm_runners):
            # Determine runners that are nominally idle and return list
            idle_runners = [
                runner
                for runner in self._bm_runners
                if not runner.jobs.list(status=opts)
            ]
        return idle_runners

    def get_pending_benchmarking(self):
        BenchmarkingConfigT = namedtuple(
            "BenchmarkingConfigT",
            ["BsMrId",
             "BsBranchRef",
             "BsRepoRef",
             "BsBranchSHA",
             "BsPipelineId",
             "TriggerTimeOfBsPipeline"
            ],
        )

        # Get MRs that could be considered as being pending. Benchmarking is non-blocking
        # MRs might be merged before benchmarking is done.
        mr_opts = ("opened", "merged")
        mrs_pending = [mr for mr in self._bs_mrs if mr.state in mr_opts]
        mr_id = {}
        for _mr in mrs_pending:
            if _mr.source_branch in mr_id:
                logging.warning("MRs with duplicate source branches found: %s and %s",
                                mr_id[_mr.source_branch], _mr.id)
            else:
                mr_id[_mr.source_branch] = _mr.id
        branches_pending = [mr.source_branch for mr in mrs_pending]
        # Get BuildStream pipelines, if running or pending then benchmarking might be triggered
        # when a pipeline is complete.
        bs_pipeline_opts = ["running", "pending"]
        bs_pipelines_in_flight = [
            pipeline
            for pipeline in self._bs_pipelines
            if pipeline.ref in set(branches_pending)
            and pipeline.status in set(bs_pipeline_opts)
        ]
        # Iterate the pipelines in flight and determine parameters for potential benchmarking
        config = [
            BenchmarkingConfigT(
                mr_id[pipeline.ref],
                pipeline.ref,
                self._bs_repo,
                pipeline.sha,
                pipeline.id,
                pipeline.created_at
            )
            for pipeline in bs_pipelines_in_flight
        ]
        return config

    def get_ready_benchmarking(self):
        BenchmarkingConfigReadyT = namedtuple(
            "BenchmarkingConfigT",
            [
                "BsMrId",
                "BsBranchRef",
                "BsRepoRef",
                "BsBranchSHA",
                "BsPipelineId",
                "TriggerTimeOfBsPipeline",
                "FinishTimeOfBsPipeline",
                "Labels",
            ],
        )

        # Get MRs that could be considered as being pending. Benchmarking is non-blocking
        # MRs might be merged before benchmarking is done.
        mr_opts = ("opened", "merged")
        # Find MRs 1st
        mrs_pending = [mr for mr in self._bs_mrs if mr.state in mr_opts]
        # Get labels for MR associated with each branch.
        # Filtering for priority etc will be done elsewhere
        mr_set = {}
        for _mr in mrs_pending:
            mr_set[_mr.source_branch] = _mr
        # Find branches
        branches_pending = [
            mr.source_branch for mr in mrs_pending if mr.state in mr_opts
        ]

        # Get BuildStream pipelines that have completed successfully
        bs_pipelines_success = [
            pipeline
            for pipeline in self._bs_pipelines
            if pipeline.ref in set(branches_pending) and pipeline.status == "success"
        ]

        # Get Benchmarking pipelines statuses
        bm_pipelines_status = [
            BenchmarkPipelineEntryStatus(pipeline) for pipeline in self._bm_pipelines
        ]

        # Get all bs_pipelines ids from benchmarking pipeline statuses
        extracted_bs_ids = [
            bm_pipeline.get_buildstream_pipeline_id
            for bm_pipeline in bm_pipelines_status
            if bm_pipeline.get_buildstream_pipeline_id() is not None
        ]

        # Get all buildstream pipelines that do not relate to current benchmarking pipelines
        bs_p_no_bm = [
            pipeline
            for pipeline in bs_pipelines_success
            if pipeline.id not in extracted_bs_ids
        ]

        # Get array of pending Buildstream branches
        bs_pending_branches = [
            pipeline.BsBranchRef for pipeline in self._benchmarking_pending
        ]

        # Filter out bs pipelines that have been superceeded by new pushes i.e. no new pipelines
        # pending for associated branch
        bs_p_id_not_sup = [
            pipeline
            for pipeline in bs_p_no_bm
            if pipeline.ref not in bs_pending_branches
        ]

        # Pick only the latest pipeline references for each branch
        pipeline_versions = {}
        for pipeline in bs_p_id_not_sup:
            if pipeline.ref in pipeline_versions:
                a_time = Status.get_time(pipeline_versions[pipeline.ref].created_at)
                b_time = Status.get_time(pipeline.created_at)
                if a_time < b_time:
                    pipeline_versions[pipeline.ref] = pipeline
            else:
                pipeline_versions[pipeline.ref] = pipeline
        # Get just the pipeline values
        pipelines_latest = list(pipeline_versions.values())

        # Create configuration based on downfiltered pipelines
        config = [
            BenchmarkingConfigReadyT(
                mr_set[pipeline.ref].id,
                pipeline.ref,
                self._bs_repo,
                pipeline.sha,
                pipeline.id,
                pipeline.created_at,
                pipeline.updated_at,
                mr_set[pipeline.ref].labels,
            )
            for pipeline in pipelines_latest
        ]
        return config

    def get_running_benchmarking(self):
        BenchmarkingConfigRunT = namedtuple(
            "BenchmarkingConfigRunT",
            [
                "BsMrId",
                "BsBranchRef",
                "BsRepoRef",
                "BsBranchSHA",
                "BsPipelineId",
                "TriggerTimeOfBsPipeline",
                "FinishTimeOfBsPipeline",
                "TriggerTimeOfBmPipeline",
            ],
        )

        # Get all bm_pipelines from benchmarking pipeline with running status and construct
        # Status wrappers
        bm_pipelines_status = [
            BenchmarkPipelineEntryStatus(bm_pipeline)
            for bm_pipeline in self._bm_pipelines
            if bm_pipeline.status == "running"
        ]

        # Get all running benchmarking status pipelines that have valid buildstream pipeline ids
        bm_valid_pipelines = [
            bm_pipeline
            for bm_pipeline in bm_pipelines_status
            if bm_pipeline.get_buildstream_pipeline_id() is not None
        ]

        # Create configuration set for current running benchmarks triggered by buildstream MRs
        config = [
            BenchmarkingConfigRunT(
                pipeline.get_buildstream_mr(),
                pipeline.get_buildstream_branch(),
                self._bs_repo,
                pipeline.get_buildstream_pipeline_sha(),
                pipeline.get_buildstream_pipeline_id(),
                pipeline.get_buildstream_pipeline_bs_pipe_start_time(),
                pipeline.get_buildstream_pipeline_bs_pipe_end_time(),
                pipeline.benchmarking_pipeline.created_at,
            )
            for pipeline in bm_valid_pipelines
        ]

        return config

    def get_complete_benchmarking(self):
        BenchmarkingConfigCompleteT = namedtuple(
            "BenchmarkingConfigCompleteT",
            [
                "BsMrId",
                "BsBranchRef",
                "BsRepoRef",
                "BsBranchSHA",
                "BsPipelineId",
                "BsMRNoteId",
                "TriggerTimeOfBsPipeline",
                "FinishTimeOfBsPipeline",
                "BmPipelineId",
                "BmPipelineStatus",
                "TriggerTimeOfBmPipeline",
                "FinishTimeOfBmPipeline",
            ],
        )

        # Define possible end states for pipelines
        bm_pipeline_opts = ["success", "failed", "canceled", "skipped"]

        # Get all bm_pipelines from benchmarking pipeline with one of the end states and construct
        # Status wrappers
        bm_pipelines_status = [
            BenchmarkPipelineEntryStatus(bm_pipeline)
            for bm_pipeline in self._bm_pipelines
            if bm_pipeline.status in bm_pipeline_opts
        ]

        # Get all ended benchmarking status pipelines that have valid buildstream pipeline ids
        bm_pl_bs_pl_val = [
            bm_pipeline
            for bm_pipeline in bm_pipelines_status
            if bm_pipeline.get_buildstream_pipeline_id() is not None
        ]

        # Create configuration set for current running benchmarks triggered by buildstream MRs
        config = [
            BenchmarkingConfigCompleteT(
                pipeline.get_buildstream_mr(),
                pipeline.get_buildstream_branch(),
                self._bs_repo,
                pipeline.get_buildstream_pipeline_sha(),
                pipeline.get_buildstream_pipeline_id(),
                pipeline.get_buildstream_mr_note_ref(),
                pipeline.get_buildstream_pipeline_bs_pipe_start_time(),
                pipeline.get_buildstream_pipeline_bs_pipe_end_time(),
                pipeline.benchmarking_pipeline.id,
                pipeline.benchmarking_pipeline.status,
                pipeline.benchmarking_pipeline.created_at,
                pipeline.benchmarking_pipeline.finished_at,
            )
            for pipeline in bm_pl_bs_pl_val
        ]

        return config

    @property
    def available_runners(self):
        """Get the available benchmarking runner list

         These are the runners that have the correct tags
         for the benchmarking gitlab ci jobs and have no
         current jobs.
      """
        return self._available_runners

    @property
    def benchmarking_pending(self):
        """Get the potential available benchmarking details

         These are potential benchmarking jobs that depend on
         the outcome of buildstream pipelines (i.e. the
         MR is tagged for benchmarking, but the buildstream
         pipeline has not completed.
      """
        return self._benchmarking_pending

    @property
    def benchmarking_ready(self):
        """Get the benchmarking details for those that could be run given resources.

         These are potential benchmarking jobs that have successfully completed
         buildstream pipeline jobs and which relate to MRs that have been tagged
         for benchmarking.
      """
        return self._benchmarking_ready

    @property
    def benchmarking_running(self):
        """Get the benchmarking details for those benchmarking pipelines that are currently
         being run.
      """
        return self._benchmarking_running

    @property
    def benchmarking_complete(self):
        """Get the benchmarking details for those benchmarking pipelines that have completed
         successfully.
      """
        return self._benchmarking_complete

    @property
    def get_bs_pipelines(self):
        """Get the buildstream pipelines.
      """
        return self._bs_pipelines

    @property
    def get_bm_pipelines(self):
        """Get the benchmarking pipelines.
      """
        return self._bm_pipelines

    @property
    def bs_mrs(self):
        """ Get the buildstream mrs.
      """
        return self._bs_mrs
