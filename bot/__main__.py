#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

# bot for orchestrating ci benchmarking

import logging
import sys
import traceback

import click

from .core import main_loop
from .config import BotConfig


@click.command()
@click.option(
    "config_file",
    "--config-file",
    "-c",
    type=click.Path(),
    default=None,
    help="YAML description of bot configuration overriding the defaults.",
    multiple=False,
)
@click.option(
    "--debug/--no-debug", default=BotConfig.default.debug(), help="Debug option"
)
@click.option(
    "--logging_path",
    type=click.Path(),
    default=BotConfig.default.logging_path(),
    help="Path for logging to be sent to, if non set will output to standard out.",
)
@click.option(
    "--dev_host",
    default="https://gitlab.com",
    help="Development environment solution https path.",
)
@click.option(
    "--bs_repo_path",
    default=BotConfig.default.bs_repo_path(),
    help="Buildstream dev path.",
)
@click.option(
    "--bm_repo_path",
    default=BotConfig.default.bm_repo_path(),
    help="Benchmarking dev path.",
)
@click.option(
    "--bs_bm_tags",
    multiple=True,
    default=BotConfig.default.bs_bm_tags(),
    help="MR tags set in dev host environment to indicate benchmarking is required "
    "and what the priority for running is in descending order.",
)
@click.option(
    "--token",
    default=BotConfig.default.token(),
    help="Authentication token required to acceess dev host environment.",
)
@click.option(
    "--runner_id",
    default=BotConfig.default.runner_id(),
    help="The runner tags that are required for benchmarking jobs.",
)
@click.option(
    "--runner_tags",
    multiple=True,
    default=["cleanup", "benchmarks", "publish"],
    help="Runner tags that need to associate with a given runner for benchmarking to "
    "run.",
)
@click.option(
    "--timeout",
    default=BotConfig.default.timeout(),
    help="Timeout between processing status (seconds)",
)
def run(**kwargs):
    """ Bot for benchmarking buildstream MRs with suitable tags
   """
    if kwargs["debug"]:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    config = BotConfig(**kwargs)

    main_loop(config)


try:
    # pylint: disable=unexpected-keyword-arg,no-value-for-parameter
    run(prog_name="bot")
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.stderr.write("Traceback: {}\n".format(traceback.format_exc()))
    sys.exit(1)
