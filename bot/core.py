#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import logging
from datetime import datetime
from time import sleep
import gitlab
import requests

from .mr import parse_mrs, update_mr_results, update_mr_failure, add_mr_note, remove_mr_labels
from .mr import update_mr_run, check_for_update, post_digest_results
from .pipeline import parse_pipelines, trigger_pipeline
from .runner import parse_runners
from .status import Status
from .benchmark_run import BenchmarkRun
from .benchmark_status import BenchmarkRequest


def get_status(config) -> Status:
    """ This gets the status of the buildstream MRs and bechmarking status.

       Args:
            config - the parsed configuration for the bot.
   """
    stat = None
    with gitlab.Gitlab(config.get_dev_host, private_token=config.get_token, api_version=4) as _gl:
        try:
            mrs = parse_mrs(config, _gl)
            bs_pipelines = parse_pipelines(config, config.get_bs_repo, _gl)
            bm_pipelines = parse_pipelines(config, config.get_bm_repo, _gl)
            bm_runners = parse_runners(config, _gl)
            stat = Status(config, mrs, bs_pipelines, bm_pipelines, bm_runners)
        except gitlab.exceptions.GitlabGetError as e:
            logging.error("Unable to get list of MRs for project: %s", e)
            return None
        except requests.exceptions.ConnectionError as _er:
            logging.error(
                "Unable to access devhost: %s \n error: %s", config.get_dev_host, _er
            )
            return None
        except gitlab.exceptions.GitlabAuthenticationError as err:
            logging.error("Authentication has failed HALTING: %s", err)
            raise
        except gitlab.exceptions.GitlabListError as ler:
            logging.error("Unable to get list from gitlab: %s", ler)
            return None
    return stat


def check_label_priority(tags, mr_label):
    """ Return the earliest (highest priority) match from the list
       of priority labels in the configuration for supplied benchmark
       tags.
   """
    rtn = None
    matches = set(tags) & set(mr_label)
    if matches:
        for counter, value in enumerate(tags):
            if value in mr_label:
                rtn = counter
                break
    return rtn


def process_ready_runs(config, status, _gl=None):
    """ This will process the benchmark requests that are ready for
        processing (i.e. the Buildstream pieline has completed and
        correct MR labels are set).

       Args:
            config - the parsed configuration for the bot.
            status - The status of benchmarking regarding buildstream MRs
                     and benchmarking runs.
            _gl (optional) - Gitlab reference.

   """
    if status is None:
        logging.error("Unable to process: No status given")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )

    # Add return for new requested benchmarks
    requested_benchmarks = []
    # Check if there are any idle runners for benchmarking
    if status.get_idle_runners():
        # Get the benchmarking requests
        benchmark_requests = status.get_ready_benchmarking()
        # Order for importance and time
        benchmark_requests = sorted(
            benchmark_requests,
            key=lambda x: (
                check_label_priority(config.get_bs_bm_tags, x.Labels),
                Status.get_time(x.TriggerTimeOfBsPipeline),
                ),
            )
        # Get the completed bs pipelines that have benchmark runs
        complete_bs_runs = [complete_run.BsPipelineId
                            for complete_run in status.benchmarking_complete]
        # Get the running bs pipelines
        running_bs_runs = [running_bm.BsPipelineId
                           for running_bm in status.benchmarking_running]

        # Work out the maximum number of requests that can be run
        num_runs = min(len(benchmark_requests), len(status.get_idle_runners()))
        del benchmark_requests[num_runs:]
        # Loop through the requests that can be processed and create the request object
        for b_request in benchmark_requests:
            try:
                # Check if bs pipeline has already been benchmarked, continue to next
                # if so.
                covered = False
                for complete in complete_bs_runs:
                    if int(complete) == int(b_request.BsPipelineId):
                        covered = True
                # Check if benchmark of bs pipeline has already started, continue to
                # next if so.
                for running in running_bs_runs:
                    if int(running) == int(b_request.BsPipelineId):
                        covered = True

                # Continue to next if benchmark does not need to be executed
                # pylint: disable=singleton-comparison
                if covered == True:
                    continue

                # Get the request object
                request = BenchmarkRequest(b_request)
                # Get the initial note reference in the MR
                note_id = add_mr_note(config, request, _gl)
                # Keep a reference to the MR note id in the benchmarking request
                # so that it can be retained in the benchmark pipeline variables for
                # future reference.
                request.set_bs_mr_note_id(note_id)
                # Trigger the benchmarking request
                pipeline_result = trigger_pipeline(config, request, _gl)
                # Generate the benchmark run object from the pipeline request result
                run_status = BenchmarkRun(pipeline_result)
                # Keep a list of benchmarking requests
                requested_benchmarks.append(run_status)
                # Update the BS MR with details of the requested benchmark run
                update_mr_run(config, run_status, request, _gl)
            except gitlab.exceptions.GitlabGetError as e:
                update_mr_failure(config, request, _gl)
                logging.error("Unable to trigger benchmark request: %s : %s", request, e)
                continue
    return requested_benchmarks


def process_completed_runs(config, status, triggered_runs, _gl=None):
    """ This will process the completed runs (updating MR information with
        results etc.

       Args:
            config - the parsed configuration for the bot.
            status - The status of benchmarking regarding buildstream MRs
                     and benchmarking runs.
            _gl (optional) - Gitlab reference.

   """
    if status is None:
        logging.error("Unable to process: No status given")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )

    # Get all completed benchmark runs and cross correlate with MRs that are still
    # pending. If completed benchmarks have been superseeded by further requested
    # runs then ignore them. If a benchmark run is pending also ignore. Update the
    # mr accordingly.
    running_benchmarking_runs = status.benchmarking_running
    pending_benchmarking_runs = status.benchmarking_pending
    complete_benchmarking_runs = status.benchmarking_complete
    # Iterate for completed benchmarking runs that don't have further
    # pending benchmark runs.
    run_set = {}
    for complete_run in complete_benchmarking_runs:
        update = complete_run
        for pending_run in pending_benchmarking_runs:
            if complete_run.BsMrId == pending_run.BsMrId:
                update = None
                break
        # If benchmarking run is pending for mr don't do any updates
        if update is not None:
            # If multiple benchmark results for same mr check for the latest
            if complete_run.BsMrId in run_set:
                stored_time = Status.get_time(run_set[complete_run.BsMrId].TriggerTimeOfBmPipeline)
                next_time = Status.get_time(complete_run.TriggerTimeOfBmPipeline)
                if stored_time < next_time:
                    run_set[complete_run.BsMrId] = complete_run
            else:
                run_set[complete_run.BsMrId] = complete_run

    # Determine which MRs still have benchmarking requests that
    # have not started or completed
    mrs_outstanding = [benchmark.BsMrId for benchmark in running_benchmarking_runs]
    mrs_pending = [benchmark.BsMrId for benchmark in pending_benchmarking_runs]
    mrs_triggered = [benchmark.get_benchmark_entry_config().get_buildstream_mr()
                     for benchmark in triggered_runs]

    for latest_complete_run in run_set.values():
        # Check if update required
        if check_for_update(status, latest_complete_run):
            # Update mrs with any results
            update_mr_results(config, status, latest_complete_run, _gl)
            post_digest_results(config, latest_complete_run, _gl)
            if ((latest_complete_run.BsMrId not in mrs_outstanding) and
                    (latest_complete_run.BsMrId not in mrs_pending) and
                    (latest_complete_run.BsMrId not in mrs_triggered)):
                # Remove any benchmarking tags from updated mrs
                remove_mr_labels(config, latest_complete_run, _gl)
    return True


def process_status(config, status):
    """ This will process the current status (launching new benchmarking
       as required and updating MRs with results.

       Args:
            config - the parsed configuration for the bot.
            status - The status of benchmarking regarding buildstream MRs
                     and benchmarking runs.
   """
    if status is None:
        logging.error("Unable to process: No status given")
        return None

    if config is None:
        logging.error("No configuration available unable to process")
        return None

    with gitlab.Gitlab(config.get_dev_host, private_token=config.get_token, api_version=4) as _gl:
        try:
            triggered_runs = process_ready_runs(config, status, _gl)
            process_completed_runs(config, status, triggered_runs, _gl)
        except gitlab.exceptions.GitlabGetError as e:
            logging.error("Unable to get list of MRs for project: %s", e)
            return None
        except requests.exceptions.ConnectionError as _er:
            logging.error(
                "Unable to access devhost: %s \n error: %s", config.get_dev_host, _er
            )
            return None
        except gitlab.exceptions.GitlabAuthenticationError as err:
            logging.error("Authentication has failed HALTING: %s", err)
            raise
        except gitlab.exceptions.GitlabListError as ler:
            logging.error("Unable to get list from gitlab: %s", ler)
            return None
    return True


def main_loop(config):
    """ This is the main loop for the bot after configuration.
       Will loop round while run state is still true. It is
       expected that try catching of exceptions will halt the
       run state (e.g. fatal authentication error).

       Args:
            config - the parsed configuration for the bot.
   """
    run = True

    while run:
        logging.info("Starting poll at: %s", datetime.now())
        # Get the current status from the Buildstream and Benchmarking development environments
        status = get_status(config)

        # Check if status has been returned
        if status is not None:
            # Process the status - i.e. trigger benchmarking, update buildstream MRs etc
            if not process_status(config, status):
                # Log error due to failure to process status correctly
                logging.error("Unable to process status")

        # Sleep required period of time before re-polling status and processing
        sleep(config.get_timeout)
