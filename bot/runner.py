#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import gitlab


def parse_runners(config, _gl=None):
    """ This will get a list of valid active benchmarking runners.
       Valid means that the runners have all the correct tags (taken
       from config file and are online)

       Args:
            config - the parsed configuration for the bot.
            gl - (optional) dev environment reference.
       Returns:
            list of active runners.
   """

    if _gl is None:
        # Get reference to relevant dev environment instance
        _gl = gitlab.Gitlab(
            config.get_dev_host, private_token=config.get_token, api_version=4
        )
    # Get runners list (all available and filter down using tags)
    runners = _gl.runners.list(all=True)
    valid_runners = [
        _x
        for _x in runners
        if set(config.get_runner_tag).issubset(set(_gl.runners.get(_x.id).tag_list))
    ]
    return valid_runners
