FROM alpine:latest

ARG REPO=https://gitlab.com/BuildStream/benchmarking/bst-benchmarking-bot.git
ARG BRANCH=master
WORKDIR /srv/bst-benchmark-bot

RUN adduser -S benchmark-bot -G nogroup --no-create-home \
    && apk add --no-cache git bash python3 \
    && git clone --no-hardlinks $REPO bst-bot --branch $BRANCH \
    && pip3 install -r bst-bot/requirements.txt

WORKDIR /srv/bst-benchmark-bot/bst-bot

RUN chown -R benchmark-bot:nogroup /srv/bst-benchmark-bot

USER benchmark-bot

# Define container execution runtime
ENTRYPOINT ["python3", "-m", "bot"]
