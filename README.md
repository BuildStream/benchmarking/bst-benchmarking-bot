BuildStream benchmarking bot.

* This will automatically benhcmark buildstream branches who are associated with
  suitably tagged MRs.

* Production usage is expected to be via docker
  Invocation via docker is expected to be as follows:
  From configuration file:
     docker run -v <config file host path>:/srv/config.yml <image name> -c /srv/config.yml
  From commandline (e.g.):
     docker run <image name> --dev_host="https://gitlab.com" \
                             --bs_repo_path="BuildStream/buildstream" \
                             --bm_repo_path="BuildStream/benchmarking/benchmarks" \
                             --bs_bm_tags 'Benchmark_Prio-1' \
                             --token <security token> \
                             --runner_id 'bl-benchmark-runner-1' \
                             --runner_tags ['cleanup','benchmarks','publish']
                             --timeout 10
